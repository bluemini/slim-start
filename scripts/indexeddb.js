var db;
var request = window.indexedDB.open("MyGraphDatabase", 5);

// request.onsuccess = function(e) {
// 	db = request.result;
// };

cyn(request).bind('success', function(e) {
	db = request.result;
	console.log(db);
});
cyn(request).bind('error', function(e) {
	console.log(e);
});

// we must handle the onupgradeneeded to do the preliminary setup on the data store
request.onupgradeneeded = function(e) {

	// a reference to the data store is passed in the event
	var db = e.target.result;

	// we setup an object store in the data store and add an index
	if (e.target.version == 1) {
		var  objectStore = db.createObjectStore("nodes", { keyPath: "node_id" });

	} else if (db.version == 5) {
		db.deleteObjectStore("nodes");
		var objectStore = db.createObjectStore("nodes", { keyPath: "node_id", autoIncrement: true });
		objectStore.createIndex("project_id", "project_id", { unique: false });
		alert("updated to v 2");
	}
};



// we bind to the 'save' button a static JS Object to the db
cyn("save").bind("click", function() {
	var projectId = cyn("projectid").text();
	if (projectId != "") {
		saveData({
			project_id: projectId,
			name: "Another job for me",
			client: "That other bit website"
		});
	};
	cyn("projectid").text("");
})

function validate

function saveData(data) {
	var transaction = db.transaction(["nodes"], "readwrite");

	transaction.oncomplete = function(e) {
		// handle the oncomplete event if necessary...
	};

	transaction.onerror = function(e) {
		// handle the error...
	};

	var objectStore = transaction.objectStore("nodes");

	// generate the data to store...this would usually be dynamic
	// var data = { node_id: 1, project_id: 1234500, name: "My First Job", client: "Big Website Required Inc." };

	// push the data into the object store.
	console.log(data);
	var insRequest = objectStore.put(data); // we use a 'put' which is similar to an upsert
											// an 'add' would be equivalent to an insert

	// create a handler for success on the data insertion
	cyn(insRequest).bind('success', function(e) {
		alert(e.target.result);
	});
}

// when you click on the 'Fetch' button (id==get), then retrieve and display the record
cyn("get").bind("click", function() {
	var transaction = db.transaction(["nodes"]);

	var objectStore = transaction.objectStore("nodes");
	var readRequest = objectStore.get(2);

	readRequest.onsuccess = function(e) {
		alert("Found: " + JSON.stringify(e.target.result));
	}
});