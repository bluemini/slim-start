var myObj = (function() {
    
    var index = 0;

    var increment = function() {
        this.idx += 1;
    };

    return {
        idx: 0,
        inc: increment
    };

}());

var myOtherObj = {};

;(function(base) {
    base.index = 0;
    base.inc = function() {
        this.index += 1;
    };
}(myOtherObj))

// on load handler
cyn().loader(function() {

    var mod1 = cyn('modules_1');
    mod1.text("Welcome " + myObj.idx);

    var button1 = cyn('button_1');
    button1.bind('click', function(e) {
        myObj.inc();
        mod1.text('myObj ' + myObj.idx)
    });

    var button2 = cyn('button_2');
    button2.bind('click', function(e) {
        myOtherObj.inc();
        mod1.text('myOtherObj ' + myOtherObj.index)
    });

});


// normal function
var obj = function(name) {
    this.name = name;
    this.age = this.name.length;
    this.sayhi = function() {
        console.log("Hi, " + this.name +"! You're " + this.age);
    }
    return { 
        sayhi: sayhi,
        name: "Bo",
        age: 2
    }
}
var myobjObj = obj("David");
myobjObj.sayhi();

window.sayhi();
console.log(window.name);


// look at the Constructor pattern
var Obj = function(name) {
    var that = {};
    this.name = name;
    this.age = this.name.length;
    this.sayhi = function() {
        console.log("Hi, " + this.name +"! You're " + this.age);
    }
    return 5;
}
var myObjObj = new Obj("Nick");

console.log(myObjObj);
myObjObj.sayhi();


var myObject = {
    name: "Nick",
    sayhi: function() {
        console.log("Hi, " + this.name)
    }
}

// when we call sayhi() on the object, it's a method type call and so within 
// the function itself, this is bound to the myObject object. Therefore 
// this.name is defined and resolves to 'Nick', so we should see in the console:
//      "Hi, Nick"
myObject.sayhi();